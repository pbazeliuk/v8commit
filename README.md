# **V8Commit** - инструмент для автоматического разборки\сборки обработок, отчетов и конфигураций для платформы 1С 8.3 в режиме управляемых форм. #

### Об инструменте ###

* Версия - 1.0.2
* [Консольная версия - 3.0.0a](https://github.com/pbazeliuk/V8Commit)

### Что к чему ###

* [Управляем версиями в «1C:Предприятие 8» (Git)](https://pbazeliuk.com/2014/02/22/git-1c-version-control-part-1/)
* [Управляем версиями и тестированием «1C:Предприятие 8» (Git, часть 2)](https://pbazeliuk.com/2014/04/24/git-1c-version-control-part-2/)
* [Git-flow в «1С:Предприятие 8»](https://pbazeliuk.com/2014/10/30/git-flow-1c-enterprise/)

### Как выйти на контакт ###

* Инструмент для проведения юнит-тестирования - [xUnitFor1C](https://github.com/xDrivenDevelopment/xUnitFor1C)
* Предложения и идеи [Базелюк Петр](mailto:pbazeliuk@gmail.com)